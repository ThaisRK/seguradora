import { StyleSheet } from "react-native";
const Styles = () => {
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      paddingTop: Constants.statusBarHeight,
    },
    figura: {
      width: 300,
      height: 150,
      margin: 15,
      resizeMode: "stretch",
    },
    textInput: {
      borderWidth: 1,
      borderColor: "#ccc",
      width: "70%",
      padding: 10,
      margin: 3,
    },
    button: {
      width: 100,
      backgroundColor: "purple",
      alignItems: "center",
      padding: 5,
      margin: 3,
      borderRadius: 5,
    },
    textButton: {
      color: "#fff",
      fontWeight: "bold",
      fontSize: 18,
      margin: 3,
    },
    row1: {
      flexDirection: "row",
      justifyContent: "space-between",
      padding: 3,
    },
  });
};

export default Styles;
