import { useEffect, useState } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  StyleSheet,
  Image,
} from "react-native";
import FormCalculo from "../FormCalculo/FormCalculo";
import Item from "../Item/Item";
import Constants from "expo-constants";
import AsyncStorage from "@react-native-async-storage/async-storage";


export default function ListaInteressados({ }) {
  const backgroundImage = require("./../../assets/seguradora.jpg");

  const [listaDeClientesInteressados, setListaDeClientesInteressados] =
    useState([]);

  const getData = async () => {
    console.log("ouaoiao");
    try {
      const jsonValue = await AsyncStorage.getItem("@storage_Key");
      const storagedClients = jsonValue != null ? JSON.parse(jsonValue) : [];
      setListaDeClientesInteressados(storagedClients);
    } catch (e) {
      // error reading value
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const renderItem = ({ item }) => {
    return (
      <>
        <Item
          id={item.id}
          client={item.client}
          marca={item.marca}
          modelo={item.modelo}
          renovacao={item.renovacao}
          avaliacao={item.avaliacao}
          valorEstimado={item.valorEstimado}
        />
      </>
    );
  };

  return (
    <View style={styles.container}>
      <Image source={backgroundImage} style={styles.figura} />
      <Text> Clientes interessados {"\n"} </Text>
      <FlatList
        data={listaDeClientesInteressados}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    paddingTop: Constants.statusBarHeight,
  },
  figura: {
    width: 300,
    height: 150,
    margin: 15,
    resizeMode: "stretch",
    borderRadius: 5,
  },
  textOutput: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "purple",
    padding: 20,
    justifyContent: "center",
    width: 300,
    height: 120,
    margin: 5,
  },
});
