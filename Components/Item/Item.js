import { Text, StyleSheet, View, Pressable } from "react-native";

const Item = ({ id, marca, renovacao, client, modelo, avaliacao, valorEstimado }) => {
  return (
    <View style={styles.textOutput}>
      <Text>ID: {id}</Text>
      <Text>Cliente: {client}</Text>
      <Text>Marca: {marca}</Text>
      <Text>Modelo: {modelo}</Text>
      <Text>Renovação: {renovacao == true ? "Sim" : "Não"}</Text>
      <Text>Avaliação: {Number(avaliacao).toFixed(2)}</Text>
      <Text>Valor estimado do seguro: {Number(valorEstimado).toFixed(2)}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  textOutput: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "purple",
    padding: 20,
    justifyContent: "center",
    width: 300,
    height: 120,
    margin: 5,
  },
});
export default Item;
