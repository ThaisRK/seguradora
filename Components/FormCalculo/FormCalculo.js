import { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";
import SelectDropdown from "react-native-select-dropdown";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import Constants from "expo-constants";
import Item from "../Item/Item";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function FormCalculo({ navigation }) {
  const seguradora = require("./../../assets/seguradora.jpg");

  const [data, setData] = useState([]);

  const [listaMarcas, setListaMarcas] = useState([
    "Chevrolet",
    "Ford",
    "Renaut",
  ]);

  const [marca, setMarca] = useState("");
  const [renovacao, setRenovacao] = useState(false);
  const [client, setClient] = useState("");
  const [modelo, setModelo] = useState("");
  const [avaliacao, setAvaliacao] = useState("");
  const [valorEstimado, setValorEstimado] = useState("");
  const [textoEstimativa, setTextoEstimativa] = useState("");
  const [selectDefault, setSelectDefault] = useState("Selecione");

  // const useNavi = useNavigation();

  const salvar = async (clientes) => {
    try {
      const jsonValue = JSON.stringify(clientes);
      await AsyncStorage.setItem("@storage_Key", jsonValue);
    } catch (e) {
      // error writing value
    }
  };

  const adicionar = () => {
    const adicionado = {
      id: new Date().getTime().toString(),
      marca: marca,
      renovacao: renovacao,
      client: client,
      modelo: modelo,
      avaliacao: avaliacao,
      valorEstimado: valorEstimado,
    };
    setData([...data, adicionado]);
    salvar([...data, adicionado]);
  };

  const calcular = () => {
    const calc = Number(avaliacao * 0.05).toFixed(2);
    setTextoEstimativa(`Valor estimado do seguro: R$ ${calc}`);
    setValorEstimado(calc);
  };

  const novo = () => {
    setSelectDefault("Selecione");
    setMarca("");
    setRenovacao("false");
    setClient("");
    setModelo("");
    setAvaliacao("");
    setValorEstimado("");
    setTextoEstimativa("");
  };

  const validador = () => {
    if (!client || !modelo || !marca || !valorEstimado) {
      setWarningText("Campo obrigatório");
      setWarningTextValor("Necessário realizar o cálculo");
    } else {
      adicionar();
    }
  };

  const [warningText, setWarningText] = useState("");
  const [warningTextValor, setWarningTextValor] = useState("");

  return (
    <View>
      <Image source={seguradora} style={styles.figura} />
      <TextInput
        style={styles.textInput}
        placeholder="Cliente / Contato"
        onChangeText={(texto) => {
          setClient(texto);
          console.log("cliente " + texto);
        }}
        value={client}
      />

      <Text style={styles.warningOutput}>{warningText}</Text>

      <TextInput
        style={styles.textInput}
        placeholder="Modelo do veículo"
        onChangeText={(texto) => {
          setModelo(texto);
          console.log("modelo " + texto);
        }}
        value={modelo}
      />

      <Text style={styles.warningOutput}>{warningText}</Text>

      <View style={styles.buttonSelect}>
        <SelectDropdown
          defaultButtonText={selectDefault}
          data={listaMarcas}
          onSelect={(selectedItem, index) => {
            setMarca(selectedItem);
            console.log(
              "marca " + selectedItem,
              index + "selecteddefaul: " + selectDefault
            );
          }}
        />

        <Text style={styles.warningOutput}>{warningText}</Text>

        <BouncyCheckbox
          textStyle={{
            textDecorationLine: "none",
          }}
          size={20}
          fillColor="purple"
          marginTop={10}
          unfillColor="#FFFFFF"
          text="É renovação"
          iconStyle={{ borderColor: "red" }}
          innerIconStyle={{ borderWidth: 2 }}
          onPress={(value) => {
            setRenovacao(value);
            console.log("checkbox " + value);
          }}
        />
      </View>
      <TextInput
        style={styles.textInput}
        keyboardType="numeric"
        placeholder="R$ Avaliação tabela FIPE"
        onChangeText={(texto) => {
          setAvaliacao(texto);
          console.log("avaliacao " + texto);
        }}
        value={avaliacao}
      />
      <Text style={styles.warningOutput}>{warningTextValor}</Text>

      <View style={styles.row1}>
        <TouchableOpacity style={styles.button} onPress={() => calcular()}>
          <Text style={styles.textButton}>Calcular</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={() => novo()}>
          <Text style={styles.textButton}>Novo</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("Lista")}
        >
          <Text style={styles.textButton}>Listar</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={styles.textOutput}>{textoEstimativa}</Text>
        <TouchableOpacity
          style={styles.largebutton}
          onPress={() => validador()}
        >
          <Text style={styles.textButton}>Registrar interesse</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginLeft: "auto",
    marginRight: "auto",
    paddingVertical: Constants.statusBarHeight,
  },
  figura: {
    width: 300,
    height: 150,
    marginTop: 20,
    marginLeft: "auto",
    marginRight: "auto",
    borderRadius: 5,
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#ccc",
    width: 300,
    padding: 10,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 7,
    borderRadius: 3,
  },
  button: {
    width: 70,
    height: 40,
    backgroundColor: "purple",
    margin: 4,
    borderRadius: 3,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  largebutton: {
    width: "90%",
    height: 40,
    backgroundColor: "purple",
    margin: 12,
    borderRadius: 3,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "auto",
    marginRight: "auto",
  },
  textButton: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 14,
  },
  row1: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "center",
  },
  buttonSelect: {
    alignItems: "center",
    borderRadius: 10,
    padding: 7,
    justifyContent: "space-evenly",
  },
  textOutput: {
    marginTop: 10,
    fontSize: 16,
    marginLeft: "auto",
    marginRight: "auto",
  },
  warningOutput: {
    marginTop: 1,
    fontSize: 12,
    marginLeft: "auto",
    marginRight: "auto",
    color: "red",
    fontStyle: "italic",
  },
});
