import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import FormCalculo from "./Components/FormCalculo/FormCalculo";
import ListaInteressados from "./Components/ListaInteressados/ListaInteressados";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Form">
        <Stack.Screen
          name="Form"
          component={FormCalculo}
          options={{
            headerStyle: {
              backgroundColor: "purple",
            },
            headerTintColor: "#FFF",
            headerShown: true,
          }}
        />
        <Stack.Screen
          name="Lista"
          component={ListaInteressados}
          options={{
            headerStyle: {
              backgroundColor: "purple",
            },
            headerTintColor: "#FFF",
            headerShown: true,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
